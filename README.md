guide project NusantaraVerse

link git
https://gitlab.com/cybercode-id/tanahairku-vr

unity versi 2022.1.13f1

model bangunan pake sketchup versi 2022/bebas, export ke .fbx atau save as .skp versi 2017
model objek pake blender, export ke .fbx
model baju pake MARVELOUS DESIGNER, export ke fbx
model terrain/jalan/map pake unity pro builder

yang perlu diperhatiin untuk 3d artist:
1. model jangan terlalu banyak faces/vertice
2. kalo ngambil model gratisan di 3d warehouse(https://3dwarehouse.sketchup.com/) pastikan faces nya ga ada yang kebalik
3. texture bisa ambil gratis di quixel bridge https://quixel.com/bridge


file audio bisa .wav/.ogg/.mp3
