using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Scenes.sandboxFariz;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class Quiz : MonoBehaviour
{
    public List<Question> QuestionsList;

    public TMP_Text titleText;
    public TMP_Text questionText;
    public TMP_Text[] answersText;
    public int currentZone = 0;
    public GameObject finishPanel;
    public TMP_Text textBenar;
    public TMP_Text textSalah;
    public TMP_Text textKeterangan;
    private int currentNumber = 0;
    private int correctAnswer = 0;
    private int wrongAnswer = 0;

    private IEnumerator coroutine;
    public void Awake()
    {
        foreach (var i in QuestionsList)
        {
            if (i.isUnlocked == true)
            {
                currentZone++;
            }
        }
        finishPanel.SetActive(false);
        currentZone--;
        currentNumber = 0;
        correctAnswer = 0;
        wrongAnswer = 0;
        titleText.text = QuestionsList[currentZone].name;
        questionText.text = QuestionsList[currentZone].question[currentNumber].question;
        for (int i = 0; i < answersText.Length; i++)
        {
            answersText[i].text = QuestionsList[currentZone].question[currentNumber].options[i];
        }
    }

    public void NextQuestion(Button button)
    {
        if (currentNumber >= 9)
        {
            if (button.GetComponentInChildren<TMP_Text>().text == QuestionsList[currentZone].question[currentNumber].answer)
            {
                correctAnswer++;
            }
            else
            {
                wrongAnswer++;
            }
            finishPanel.SetActive(true);
            textBenar.text = correctAnswer.ToString();
            textSalah.text = wrongAnswer.ToString();
            if (correctAnswer >= 7)
            {
                QuestionsList[currentZone + 1].isUnlocked = true;
                textKeterangan.text = "Map Baru Telah Terbuka";
            }
            else
            {
                textKeterangan.text = "Coba Lagi yaaa";
            }
        }
        else
        {
            if (button.GetComponentInChildren<TMP_Text>().text == QuestionsList[currentZone].question[currentNumber].answer)
            {
                coroutine = AnswerCorrection(Color.green, button);
                StartCoroutine(coroutine);
                correctAnswer++;
            }
            else
            {
                coroutine = AnswerCorrection(Color.red, button);
                StartCoroutine(coroutine);
                wrongAnswer++;
            }
        }
    }

    private IEnumerator AnswerCorrection(Color color, Button button)
    {
        button.interactable = false;
        ColorBlock cb = button.colors;
        cb.disabledColor = color;
        button.colors = cb;
        yield return new WaitForSeconds(1);
        button.interactable = true;
        currentNumber++;
        questionText.text = QuestionsList[currentZone].question[currentNumber].question;
        for (int i = 0; i < answersText.Length; i++)
        {
            answersText[i].text = QuestionsList[currentZone].question[currentNumber].options[i];
        }
    }
}
