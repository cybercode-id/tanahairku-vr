using System;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

namespace DefaultNamespace
{
    [Serializable]
    public struct Questions
    {
        public string question;
        public string[] options;
        public string answer;
    }
    
    [CreateAssetMenu]
    public class Question : ScriptableObject
    {
        [SerializeField] public List<Questions> question;
        public bool isUnlocked;
    }
}
