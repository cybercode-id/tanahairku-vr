using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lockPos : MonoBehaviour
{
    public float lockYPosition;
    private Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {
        pos=transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        pos = transform.position;
        pos.y=lockYPosition;
        transform.position = pos;
    }
}
