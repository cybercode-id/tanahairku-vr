using BNG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAngklung : MonoBehaviour
{
    public AudioClip anglungSound;
    public AudioSource audio;
    private Rigidbody rb;
    private bool bussy;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       
        if ((rb.velocity.magnitude > 4 || rb.angularVelocity.magnitude>7) && !bussy)
        {
            StartCoroutine(playSound());
        }

    }

    IEnumerator playSound()
    {
        bussy = true;
        audio.PlayOneShot(anglungSound);
        yield return new WaitForSeconds(anglungSound.length - 0.5f);
        bussy = false;
    }
}
