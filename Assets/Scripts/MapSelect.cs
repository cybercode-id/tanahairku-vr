using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MapSelect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]private GameObject daerah;
    [SerializeField] private Material selected;
    [SerializeField] private Renderer daerahMesh;

    [SerializeField] private float smoothtime = 0.6f;

    [SerializeField] private bool multiMesh;
    [SerializeField] private List<Renderer> multiDaerahMesh;

    public Vector3 scaleVal = new Vector3(2.6f, 2.6f, 2.6f);
    public float posVal;

    private float target;
    private Vector3 targetScale;
    float yVelocity = 0.0f;
    private Vector3 velocity = Vector3.zero;

    private float _localPos;
    private Vector3 _localScale;
    

    private Material _curentMat;
    

    private void Start()
    {
        _curentMat = daerahMesh.material;
        _localPos = daerah.transform.localPosition.y;
        _localScale = daerah.transform.localScale;
        target = _localPos;
        targetScale = _localScale;
    }

    void changeColorSelected()
    {
        if (!multiMesh)
        {
            daerahMesh.material = selected;
        }
        else
        {
            foreach(var val in multiDaerahMesh)
            {
                val.material = selected;
            }
        }
    }
    void changeColorUnSelected()
    {
        if (!multiMesh)
        {
            daerahMesh.material = _curentMat;
        }
        else
        {
            foreach (var val in multiDaerahMesh)
            {
                val.material = _curentMat;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        target = posVal;
        targetScale = scaleVal;
        changeColorSelected();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        target = _localPos;
        targetScale = _localScale;
        changeColorUnSelected();
    }

    private void Update()
    {
        if (target == _localPos)
        {
            float newPos = Mathf.SmoothDamp(daerah.transform.localPosition.y, target, ref yVelocity, smoothtime);
            daerah.transform.localPosition = new Vector3(daerah.transform.localPosition.x, newPos, daerah.transform.localPosition.z);
            Vector3 newScale = Vector3.SmoothDamp(daerah.transform.localScale, targetScale, ref velocity, smoothtime);
            daerah.transform.localScale = newScale;
        }
        else
        {
            float newPos = Mathf.SmoothDamp(daerah.transform.localPosition.y, target, ref yVelocity, smoothtime);
            daerah.transform.localPosition = new Vector3(daerah.transform.localPosition.x, newPos, daerah.transform.localPosition.z);
            Vector3 newScale = Vector3.SmoothDamp(daerah.transform.localScale, targetScale, ref velocity, smoothtime);
            daerah.transform.localScale = newScale;
        }
        
    }

}
