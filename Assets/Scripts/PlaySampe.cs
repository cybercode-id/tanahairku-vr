using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySampe : MonoBehaviour
{
    public AudioSource audio;
    private bool _busy;

    private void OnTriggerEnter(Collider other)
    {
        if (_busy) return;
        PlaySound();
    }

    private void OnTriggerExit(Collider other)
    {
        _busy = false;
    }

    private void OnTriggerStay(Collider other)
    {
        _busy = true;
    }

    private void PlaySound()
    {
        _busy = true;
        audio.PlayOneShot(audio.clip);
    }
}
