using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CekToggle : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite toggleSprite;
    private Sprite bg;
    Image ui_sprite;
    void Start()
    {
        ui_sprite = GetComponent<Image>();
        bg = ui_sprite.sprite;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (DashboardController.debugMode)
        {
            ui_sprite.sprite = toggleSprite;
        }
        else
        {
            ui_sprite.sprite = bg;
        }
        
    }
}
