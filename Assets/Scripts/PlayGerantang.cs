using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayGerantang : MonoBehaviour
{
    public AudioSource[] audio;

    private bool _busy;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "0")
        {
            StartCoroutine(playSound(0));
        }
        if (collision.gameObject.name == "1")
        {
            StartCoroutine(playSound(1));
        }
        if (collision.gameObject.name == "2")
        {
            StartCoroutine(playSound(2));
        }
        if (collision.gameObject.name == "3")
        {
            StartCoroutine(playSound(3));
        }
        if (collision.gameObject.name == "4")
        {
            StartCoroutine(playSound(4));
        }
        if (collision.gameObject.name == "5")
        {
            StartCoroutine(playSound(5));
        }
        if (collision.gameObject.name == "6")
        {
            StartCoroutine(playSound(6));
        }
        if (collision.gameObject.name == "7")
        {
            StartCoroutine(playSound(7));
        }
        if (collision.gameObject.name == "8")
        {
            StartCoroutine(playSound(8));
        }
        if (collision.gameObject.name == "9")
        {
            StartCoroutine(playSound(9));
        }
        if (collision.gameObject.name == "10")
        {
            StartCoroutine(playSound(10));
        }
        if (collision.gameObject.name == "11")
        {
            StartCoroutine(playSound(11));
        }
        if (collision.gameObject.name == "12")
        {
            StartCoroutine(playSound(12));
        }
    }

    IEnumerator playSound(int index)
    {
        _busy = true;
        audio[index].PlayOneShot(audio[index].clip);
        yield return new WaitForSeconds(audio[index].clip.length - 0.5f);
        _busy = false;
    }
    
}
