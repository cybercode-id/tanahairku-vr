using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadPosition : MonoBehaviour
{
    private GameObject head;
    public bool onlyOnce;

    public static int count=0;
    // Start is called before the first frame update
    void Start()
    {
        if (count < 1)
        {
            head = GameObject.FindGameObjectWithTag("MainCamera");
            transform.SetParent(head.transform);
            Vector3 target = new Vector3(head.transform.position.x, head.transform.position.y - 0.08f, head.transform.position.z + 0.6f);
            this.transform.position = target;
            if (onlyOnce)
            {
                count++;
            }
        }
        else
        {
            this.GetComponent<Animator>().enabled=false;
        }
        
        

    }


}
