using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothLookUp : MonoBehaviour
{
    public int speed = 5;
    [SerializeField] private CheckCollision checker;
    GameObject TargetPlayer;

    void Start()
    {
        TargetPlayer = GameObject.FindGameObjectWithTag("MainCamera").gameObject;
    }


    void FixedUpdate()
    {
        if (checker.PlayerInRange)
        {
            var targetRotation = Quaternion.LookRotation(TargetPlayer.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);
        }
        else { 
            transform.rotation = Quaternion.identity; 
        }
        
    }
}
