using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayGeundrang : MonoBehaviour
{
    public AudioSource audio;
    private bool _busy;

    private void OnTriggerEnter(Collider other)
    {
        if (_busy) return;
        PlaySound();
    }

    private void OnTriggerExit(Collider other)
    {
        _busy = false;
    }

    private void OnTriggerStay(Collider other)
    {
        _busy = true;
    }

    private void PlaySound()
    {
        _busy = true;
        audio.PlayOneShot(audio.clip);
    }
    
    IEnumerator playSound()
    {
        _busy = true;
        audio.PlayOneShot(audio.clip);
        Debug.Log("test");
        yield return new WaitForSeconds(audio.clip.length - 0.5f);
        _busy = false;
    }
}
