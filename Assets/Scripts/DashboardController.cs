using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashboardController : MonoBehaviour
{
    public bool isOpen;
    public static bool debugMode;
    [SerializeField] private Animator anim;
    [SerializeField] private AudioSource audio;
    [SerializeField] private List<AudioClip> clip;
    [SerializeField] private GameObject Debuger;
    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void openDash()
    {
        if (!isOpen)
        {
            audio.PlayOneShot(clip[0]);
            anim.SetTrigger("Open");
            isOpen = true;
        }
        
    }

    public void closeDash()
    {
        audio.PlayOneShot(clip[1]);
        anim.SetTrigger("Close");
        isOpen=false;
    }

    public void openSettings()
    {
        audio.PlayOneShot(clip[0]);
        anim.SetBool("Settings", true);
    }

    public void closeSettings()
    {
        audio.PlayOneShot(clip[1]);
        anim.SetBool("Settings", false);
    }

    public void cekToggleDebug(Toggle _togle)
    {
        if (!debugMode)
        {
            debugMode = _togle.isOn;
            Debuger.SetActive(debugMode);
        }
        else
        {
            debugMode = !debugMode;
            Debuger.SetActive(debugMode);
        }
    }


}
