﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KetosGames.SceneTransition;
using NUnit.Framework;
using UnityEngine.UI;
using DefaultNamespace;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace KetosGames.SceneTransition.Example
{
    public class GoScript : MonoBehaviour
    {
        public string ToScene;
        public Question Question;
        public Animator Animator;

        public void GoToNextScene()
        {
            SceneLoader.LoadScene(ToScene);
        }

        public void Checking()
        {
            if (Question.isUnlocked == true)
            {
                GoToNextScene();
            }
            else
            {
                Debug.Log("Belum Terbuka");
                Animator.SetTrigger("open_info");
            }
        }
    }
}
